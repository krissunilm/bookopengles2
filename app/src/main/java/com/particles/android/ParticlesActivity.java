package com.particles.android;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;


public class ParticlesActivity extends ActionBarActivity {

    private static final String TAG = ParticlesActivity.class.getSimpleName();


    private GLSurfaceView glSurfaceView;
    private boolean rendererSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        //setContentView(R.layout.activity_first_open_glproject);

        Log.d(TAG, "onCreate() Restoring previous state");


        glSurfaceView = new GLSurfaceView(this);

        final ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo =
                activityManager.getDeviceConfigurationInfo();

        final boolean supportsEs2 =
                configurationInfo.reqGlEsVersion >= 0x20000
                        || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1
                        && (Build.FINGERPRINT.startsWith("generic")
                        || Build.FINGERPRINT.startsWith("unknown")
                        || Build.MODEL.contains("google_sdk")
                        || Build.MODEL.contains("Emulator")
                        || Build.MODEL.contains("Android SDK built for x86")));



        final ParticlesRenderer particlesRenderer = new ParticlesRenderer(this);

        if(supportsEs2) {

            glSurfaceView.setEGLContextClientVersion(2);

            glSurfaceView.setRenderer(particlesRenderer);
            rendererSet = true;

        } else {
            Toast.makeText(this,"No Es2 sorry!", Toast.LENGTH_LONG).show();
            return;
        }



        //Adding touch to pan around the skybox
        glSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            float previousX
                    ,
                    previousY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event != null) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        previousX = event.getX();
                        previousY = event.getY();
                    } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        final float deltaX = event.getX() - previousX;
                        final float deltaY = event.getY() - previousY;
                        previousX = event.getX();
                        previousY = event.getY();
                        glSurfaceView.queueEvent(new Runnable() {
                            @Override
                            public void run() {
                                particlesRenderer.handleTouchDrag(
                                        deltaX, deltaY);
                            }
                        });
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });


        setContentView(glSurfaceView);


      }


                    @Override
                    public boolean onCreateOptionsMenu (Menu menu){
                        // Inflate the menu; this adds items to the action bar if it is present.
                        getMenuInflater().inflate(R.menu.menu_first_open_glproject, menu);
                        return true;
                    }

                    @Override
                    public boolean onOptionsItemSelected (MenuItem item){
                        // Handle action bar item clicks here. The action bar will
                        // automatically handle clicks on the Home/Up button, so long
                        // as you specify a parent activity in AndroidManifest.xml.
                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.action_settings) {
                            return true;
                        }

                        return super.onOptionsItemSelected(item);
                    }


                protected void onPause () {
                    super.onPause();

                    if (rendererSet) {
                        glSurfaceView.onPause();
                    }


                }


                protected void onResume () {
                    super.onResume();
                    if (rendererSet) {
                        glSurfaceView.onResume();
                    }
                }


            }
